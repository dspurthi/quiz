import Vue from 'vue'
import VueRouter from "vue-router";
import App from './App.vue'
import { routes } from './routes';
Vue.use(VueRouter);

const router = new VueRouter({
  routes
})

Vue.http.options.root = 'https://vuejs-http-db5fa.firebaseio.com/data.json'
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
